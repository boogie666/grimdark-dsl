(ns grimdark-dsl.spec
  (:require [clojure.spec.alpha :as s]))

(s/def ::defwarband
  (s/cat :prefix #{'defwarband}
         :name string?))

(s/def ::replace
  (s/cat :prefix #{'replace}
         :item string?
         :with #{'with}
         :replacement string?))

(s/def ::replace-multi
  (s/cat :prefix #{'replace}
         :item (s/coll-of string? :kind set?)
         :with #{'with}
         :replacement (s/or :multi-items (s/coll-of string? :kind set?)
                            :single-item string?)))


(s/def ::attach
  (s/cat :prefix #{'attach}
         :item string?
         :with #{'to}
         :attachment string?))

(s/def ::add
  (s/cat :prefix #{'add}
         :item string?))

(s/def ::action
  (s/or :replace ::replace
        :replace-multi ::replace-multi
        :add ::add
        :attach ::attach))

(s/def ::defunit
  (s/cat :prefix #{'defunit}
         :name string?
         :actions (s/* ::action)))

(s/def ::defunits
  (s/cat :prefix #{'defunits}
         :count pos-int?
         :name string?
         :actions (s/* ::action)))

(s/def ::expression
  (s/or :defwarband ::defwarband
        :defunit ::defunit
        :defunits ::defunits))

(s/def ::program
  (s/coll-of ::expression))

(s/def ::total-cost pos-int?)
(s/def ::warband-rules (s/map-of string? string?))

(s/def ::rule
  (s/and (s/or :simple-rule string?
               :vector-rule (s/coll-of string? :kind vector? :count 1)
               :value-rule (s/tuple string? pos-int?))
         (s/conformer
          (fn [[type value]]
            (case type
              :simple-rule value
              :vector-rule (first value)
              :value-rule (str (first value) "(" (second  value) ")"))))))

(s/def ::rules
  (s/coll-of ::rule))

(s/def ::name string?)
(s/def ::quality pos-int?)
(s/def ::defence pos-int?)
(s/def ::cost pos-int?)

(s/def ::equipment-item
  (s/cat :name string?
         :range (s/? pos-int?)
         :attack (s/? #(and (string? %) (= \A (first %))))
         :rules (s/* ::rule)))

(s/def ::equipment
  (s/coll-of ::equipment-item))

(s/def ::unit
  (s/keys :req-un [::equipment ::rules ::quality ::defence ::cost ::name]))

(s/def ::units
  (s/coll-of ::unit))

(s/def ::output
  (s/keys :req-un [::units ::total-cost ::warband-rules]))

(defmulti ^:private extract-data first)

(defmethod extract-data :defunit [[_ {:keys [name actions]}]]
  {:define :unit
   :name name
   :count 1
   :actions (mapcat extract-data actions)})

(defmethod extract-data :defunits [[_ {:keys [name count actions]}]]
  {:define :unit
   :name name
   :count count
   :actions (mapcat extract-data actions)})

(defmethod extract-data :replace [[_ {:keys [item replacement]}]]
  [{:action :replace
    :items #{item}
    :replacements #{replacement}}])

(defmethod extract-data :replace-multi [[_ {:keys [item replacement]}]]
  [{:action :replace
    :items item
    :replacements (extract-data replacement)}])

(defmethod extract-data :single-item [[_ item]]
  #{item})

(defmethod extract-data :multi-items [[_ items]]
  items)

(defmethod extract-data :attach [[_ {:keys [item attachment]}]]
  [{:action :attach
    :item item
    :attachment attachment}])

(defmethod extract-data :add [[_ {:keys [item]}]]
  [{:action :add
    :item item}])

(defmethod extract-data :defwarband [[_ {:keys [name]}]]
  {:define :warband :name name})

(defn parse [form]
  (let [result (s/conform ::program form)]
    (if (= ::s/invalid result)
      (throw (ex-info (s/explain-str ::program form)
                      (s/explain-data ::program form)))
      (map extract-data result))))

(comment
 (parse '[(defwarband "Battle Brothers")

          (defunit "Champion"
            (attach "Plasma Rifle" to "Assault Rifle")) 

          (defunit "Battle Brother"
            (replace "Assault Rifle" with "Heavy Machinegun"))

          (defunit "Battle Brothers"
            (add "Jetpack")
            (replace #{"Assault Rifle" "CCW"} with "2x Energy Claws"))

          (defunit "Battle Brothers"
            (add "Jetpack"))]))


(parse '[(defunit "Battle Brothers"
            (add "Jetpack")
            (replace #{"Assault Rifle" "CCW"} with "2x Energy Claws"))])
