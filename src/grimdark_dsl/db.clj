(ns grimdark-dsl.db)

(defn get-unit [db name]
  (get-in db [:units name]))

(defn- get-upgrades [{:keys [units upgrades]} name]
  (apply concat (vals (select-keys upgrades
                                   (get-in units [name
                                                  :upgrades])))))

(defn- get-replacements [upgrades]
  (->> upgrades
       (filter :replace)
       (group-by :replace)))

(defn- get-attachments [upgrades]
  (->> upgrades
       (filter :attach)
       (group-by :attach)))

(defn- get-additions [upgrades]
  (->> upgrades
       (filter :upgrade)))

(defn get-unit-data [db name]
  (let [upgrades (get-upgrades db name)
        additition (get-additions upgrades)
        replacements (get-replacements upgrades)
        attachments (get-attachments upgrades)]
     {:attachments attachments
      :additions additition
      :replacements replacements}))

(defn has-item? [unit item]
  (seq (filter
        (fn [equipment]
          (= (:name equipment) item))
        (:equipment unit))))

(defn- find-item [attachments what item-name]
  (first (for [a attachments
               e (what a)
               :when (= (:name e) item-name)]
           a)))

(defn find-equipment [items item-name]
  (find-item items :equipment item-name))

(defn find-upgrade [items item-name]
  (find-item items :upgrade item-name))





