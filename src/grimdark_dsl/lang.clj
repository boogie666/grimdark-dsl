(ns grimdark-dsl.lang
  (:require [grimdark-dsl.db :as db]
            [clojure.string :as str]))

(defmulti ^:private apply-action (fn [_ _ {:keys [action]}] action))

(defmethod apply-action :attach [unit {:keys [attachments]} {:keys [item attachment]}]
  (if-not (db/has-item? unit attachment)
    unit
    (let [full-item (db/find-equipment (get attachments attachment) item)]
      (-> unit
          (update :equipment concat (:equipment full-item))
          (update :cost + (:cost full-item))))))

(defmethod apply-action :add [unit {:keys [additions]} {:keys [item]}]
  (if-let [full-item (db/find-upgrade additions item)]
    (-> unit
        (update :equipment concat (:upgrade full-item))
        (update :cost + (:cost full-item)))
    unit))

(defmethod apply-action :replace [unit
                                  {all-replacements :replacements}
                                  {:keys [items replacements]}]
  (if-not (every? #(db/has-item? unit %) items)
    unit
    (let [valid-replacements (get all-replacements items)
          i (map #(db/find-equipment valid-replacements %) replacements)
          unit-items (into #{}
                           (comp
                            (map #(db/find-equipment [unit] %))
                            (mapcat :equipment))
                           items)]
      (-> unit
          (update :equipment concat (mapcat :equipment i))
          (update :equipment #(remove unit-items %))
          (update :cost #(apply + % (map :cost i)))))))

(defn- process-unit [db {:keys [name actions]}]
  (let [unit (db/get-unit db name)
        data (db/get-unit-data db name)
        processed-unit (reduce (fn [unit action]
                                 (apply-action unit data action))
                               unit actions)]
    (-> processed-unit
        (select-keys [:qua :def :equipment :rules :cost])
        (assoc :name name))))

(def ^:private specials
  #{#"Deadly\(\d*\)"
    #"Impact\(\d*\)"
    #"Tough\(\d*\)"
    #"Poison"
    #"Slow"
    #"Hero"
    #"Deadly"
    #"Immobile"
    #"Tough"
    #"Regeneration"
    #"Blast"
    #"Scout"
    #"Fearless"
    #"Impact"
    #"Flying"
    #"Strider"
    #"Ambush"
    #"Indirect"
    #"Rending"
    #"Fast"
    #"Furious"
    #"Fear"
    #"Sniper"
    #"Stealth"
    #"Relentless"})

(defn- matches-any? [res item]
  (some #(re-matches % item) res))

(defn- special-rule? [rule]
  (when-let [special (matches-any? specials rule)]
    (str/replace special #"\(\d*\)" "")))

(defn- army-special-rule? [{:keys [rules]} rule]
  (let [rules (into #{} (comp
                         (map first)
                         (map re-pattern))
                    rules)]
    (when-let [special (matches-any? rules rule)]
      (str/replace special #"\(\d*\)" ""))))

(defn process [db units]
  (let [units (into [] (map #(process-unit db %)) units)
        total-cost (transduce (map :cost) + 0 units)
        rules (into #{} (comp (mapcat (fn [unit]
                                        (into (:rules unit)
                                              (mapcat :stats)
                                              (:equipment unit))))
                              (keep #(or (special-rule? %)
                                         (army-special-rule? db %))))
                    units)]
    {:units units
     :rules rules
     :total-cost total-cost}))

(comment

  (time
   (-> db
       (process [test-data-2 test-data])))

  (def test-data
    {:define :unit,
     :name "Champion",
     :count 1,
     :actions
     '({:action :attach,
        :item "Plasma Rifle",
        :attachment "Assault Rifle"}
       {:action :add,
        :item "War Chant"})})

  (def test-data-2
    {:define :unit,
     :name "Battle Brother",
     :count 1,
     :actions
     '({:action :add, :item "Jetpack"}
       {:action :replace,
        :items #{"Assault Rifle" "CCW"},
        :replacements #{"2x Energy Claws"}})})

  (def db
    {:units {"Battle Brother" {:qua 3 :def 2
                               :equipment [{:name "Assault Rifle"
                                            :stats ["24\"" "A1"]}
                                           {:name "CCW"
                                            :stats ["A1"]}]
                               :upgrades #{"A" "F"}
                               :rules #{"Fearless"}
                               :cost 30}
             "Pathfinder" {:qua 3 :def 4
                           :equipment [{:name "Assault Rifle"
                                        :stats ["24\"" "A1"]}
                                       {:name "CCW"
                                        :stats ["A1"]}]
                           :upgrades #{"A" "E"}
                           :rules #{"Fearless" "Strider"}
                           :cost 30}
             "Assault Brother" {:qua 3 :def 2
                                :equipment [{:name "Pistol"
                                             :stats ["12\"" "A1"]}
                                            {:name "CCW"
                                             :stats ["A2"]}]
                                :upgrades #{"A" "H"}
                                :rules #{"Fearless"}
                                :cost 30}
             "Support Brother" {:qua 3 :def 2
                                :equipment [{:name "Assault Rifle"
                                             :stats ["24\"" "A1"]}
                                            {:name "CCW"
                                             :stats ["A1"]}]
                                :upgrades #{"A" "H"}
                                :rules #{"Fearless" "Relentless"}
                                :cost 35}
             "Brother Biker" {:qua 3 :def 2
                              :equipment [{:name "Assault Rifle"
                                           :stats ["24\"" "A1"]}
                                          {:name "CCW"
                                           :stats ["A1"]}]
                              :upgrades #{"A" "I"}
                              :rules #{"Fast"
                                       "Fearless"
                                       "Impact(1)"
                                       "Scout"}
                              :cost 45}
             "Pathfinder Biker" {:qua 3 :def 4
                                 :equipment [{:name "Assault Rifle"
                                              :stats ["24\"" "A1"]}
                                             {:name "CCW"
                                              :stats ["A1"]}]
                                 :upgrades #{"A" "J"}
                                 :rules #{"Fast"
                                          "Fearless"
                                          "Impact(1)"}
                                 :cost 40}
             "Support Biker" {:qua 3 :def 2
                              :equipment [{:name "Heavy Flamethrower"
                                           :stats ["12\"" "A6"
                                                   "AP(1)"]}
                                          {:name "Assault Rifle"
                                           :stats ["24\"" "A1"]}
                                          {:name "CCW"
                                           :stats ["A1"]}]
                              :upgrades #{"A" "K"}
                              :rules #{"Fast"
                                       "Fearless"
                                       "Impact(3)"
                                       "Tough(3)"}
                              :cost 115}
             "Destroyer" {:qua 3 :def 2
                          :equipment [{:name "Storm Rifle"
                                       :stats ["24\"" "A2"]}
                                      {:name "Energy Fist"
                                       :stats ["A2" "AP(3)"]}]
                          :upgrades #{"L"}
                          :rules #{"Ambush"
                                   "Fearless"
                                   "Tough(3)"}
                          :cost 100}
             "Champion" {:qua 3 :def 2
                         :equipment [{:name "Assault Rifle"
                                      :stats ["24\"" "A1"]}
                                     {:name "CCW"
                                      :stats ["A1"]}]
                         :upgrades #{"A" "B" "C"}
                         :rules #{"Fearless" "Hero" "Tough(3)"}
                         :cost 80}
             "Captain" {:qua 3 :def 2
                        :equipment [{:name "Assault Rifle"
                                     :stats ["24\"" "A1"]}
                                    {:name "CCW"
                                     :stats ["A1"]}]
                        :upgrades #{"A" "B" "C"}
                        :rules #{"Fearless" "Hero"
                                 "Tough(3)" "Relentless"}
                        :cost 90}
             "Engineer" {:qua 3 :def 2
                         :equipment [{:name "Assault Rifle"
                                      :stats ["24\"" "A1"]}
                                     {:name "CCW"
                                      :stats ["A1"]}]
                         :upgrades #{"A" "B"}
                         :rules #{"Fearless" "Hero"
                                  "Tough(3)" "Repair"}
                         :cost 90}
             "Psychic" {:qua 3 :def 2
                        :equipment [{:name "Assault Rifle"
                                     :stats ["24\"" "A1"]}
                                    {:name "CCW"
                                     :stats ["A1"]}]
                        :upgrades #{"A" "B" "D"}
                        :rules #{"Fearless" "Hero"
                                 "Tough(3)" "Psychic(1)"}
                        :cost 100}}
     :rules {"War Chant" "The hero and all friendly units within 12\" get +1 attach when charging"
             "Veteran Infantry" "This model gets +1 to it's attack roles for melee and shooting"
             "Shield Wall" "Enemies get -1 to hit when they attack units where all models have this rule."
             "Repair" "Once per turn if within 2\" of a unit with Tough, roll one die, on 2+ you may repair 1 wound from the target"
             "Medical Training" "This model and friendly units within 12\" get the Regeneration rule."
             "Advanced Tactics" "The hero and all friendly units within 12\" get +3\" range when shooting and +3\" ramge when using charge actions."}
     :upgrades {"A" [{:equipment [{:name "Pistol" :stats ["12\"", "A1"]}
                                  {:name "CCW" :stats ["A2"]}]
                      :replace #{"Assault Rifle" "CCW"}}
                     {:equipment [{:name "Heavy Chainsaw Sword"
                                   :stats ["A6", "AP(1)"]}]
                      :replace #{"Assault Rifle" "CCW"}
                      :cost 10}
                     {:equipment [{:name "2x Energy Claws"
                                   :stats ["A2", "AP(1)", "Rending"]}]
                      :replace #{"Assault Rifle" "CCW"}
                      :cost 10}
                     {:equipment [{:name "Gravity Pistol"
                                   :stats ["12\"" "A2" "Rending"]}]
                      :replace #{"Pistol"}
                      :cost 5}
                     {:equipment [{:name "Plasma Pistol"
                                   :stats ["12\"" "A1" "AP(2)"]}]
                      :replace #{"Pistol"}
                      :cost 5}
                     {:equipment [{:name "Storm Rifle"
                                   :stats ["24\"" "A2"]}]
                      :replace #{"Pistol"}
                      :cost 5}
                     {:equipment [{:name "Combat Shield"
                                   :stats ["Shield Wall"]}]
                      :replace #{"Pistol"}
                      :cost 10}
                     {:equipment [{:name "Energy Sword"
                                   :stats ["A2" "AP(1)" "Rending"]}]
                      :replace #{"CCW"}
                      :cost 5}
                     {:equipment [{:name "Energy Fist"
                                   :stats ["A2" "AP(3)"]}]
                      :replace #{"CCW"}
                      :cost 10}
                     {:equipment [{:name "Energy Hammer"
                                   :stats ["A2" "AP(1)" "Deadly(3)"]}]
                      :replace #{"CCW"}
                      :cost 10}
                     {:equipment [{:name "Chainsaw Fist"
                                   :stats ["A4" "AP(3)"]}]
                      :replace #{"CCW"}
                      :cost 20}
                     {:equipment [{:name "Gravity Rifle"
                                   :stats ["18\"" "A2" "Rending"]}]
                      :attach "Assault Rifle"
                      :cost 10}
                     {:equipment [{:name "Plasma Rifle"
                                   :stats ["24\"" "A1" "AP(2)"]}]
                      :attach "Assault Rifle"
                      :cost 15}
                     {:equipment [{:name "Flamethrower"
                                   :stats ["12\"" "A6"]}]
                      :attach "Assault Rifle"
                      :cost 15}
                     {:equipment [{:name "Fusion Rifle"
                                   :stats ["12\"" "A1" "AP(4)" "Deadly(6)"]}]
                      :attach "Assault Rifle"
                      :cost 25}]
                "B" [{:equipment [{:name "Jetpack"
                                   :stats ["Ambush" "Flying"]}]
                      :cost 10}
                     {:equipment [{:name "Combat Bike"}
                                  {:name "Twin Assault Rifle"
                                   :stats ["24\"" "A2"]}]
                      :cost 20}
                     {:equipment [{:name "Destroyer Armor"
                                   :stats ["Ambush" "Tough(+3)"]}]
                      :cost 80}
                     {:upgrade [{:name "Veteran Infantry"
                                 :stats ["Veteran Infantry"]}]
                      :cost 10}]
                "C" [{:upgrade [{:name "War Chant"
                                 :stats ["War Chant"]}]
                      :cost  10}
                     {:upgrade [{:name "Advanced Tactics"
                                 :stats ["Advanced Tactics"]}]
                      :cost  45}]
                "D" [{:upgrade {:name "Psychic(2)"
                                :stats ["Psychic(2)"]}
                      :replace #{"Psychic(1)"}
                      :cost 15}]
                "E" [{:equipment [{:name "Stealth Cloak"
                                   :stats ["Stealth"]}]
                      :cost 5}
                     {:upgrade [{:name "Forward Sentry"
                                 :stats ["Scout"]}]
                      :cost 10}
                     {:equipment [{:name "Shotgun"
                                   :stats ["12\"" "A2"]}]
                      :replace #{"Assault Rifle"}}
                     {:equipment [{:name "Gravity Rifle"
                                   :stats ["18\"" "A2" "Rending"]}]
                      :replace #{"Assault Rifle"}
                      :cost 5}
                     {:equipment [{:name "Plasma Rifle"
                                   :stats ["24\"" "A1" "AP(2)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 5}
                     {:equipment [{:name "Flamethrower"
                                   :stats ["12\"" "A6"]}]
                      :replace #{"Assault Rifle"}
                      :cost 10}
                     {:equipment [{:name "Sniper Rifle"
                                   :stats ["36\"" "A1" "AP(1)" "Sniper"]}]
                      :replace #{"Assault Rifle"}
                      :cost 15}
                     {:equipment [{:name "Heavy Machinegun"
                                   :stats ["36\"" "A3" "AP(1)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 25}
                     {:equipment [{:name "Missile Launcher"
                                   :stats [" * HE" "48\"" "A1" "Blast(3)" "<br/>"
                                           " * AT" "48\"" "A1" "AP(3)" "Deadly(3)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 35}]
                     


                "F" [{:upgrade [{:name "Veteran Infantry"
                                 :stats ["Veteran Infantry"]}]
                      :cost 10}
                     {:upgrade [{:name "Battle Standard"
                                 :stats ["Fear"]}]
                      :cost 20}
                     {:upgrade [{:name "Medical Training"
                                 :stats ["Medical Training"]}]
                      :cost 45}

                     {:equipment [{:name "Gravity Rifle"
                                   :stats ["18\"" "A2" "Rending"]}]
                      :replace #{"Assault Rifle"}
                      :cost 5}
                     {:equipment [{:name "Plasma Rifle"
                                   :stats ["24\"" "A1" "AP(2)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 5}
                     {:equipment [{:name "Flamethrower"
                                   :stats ["12\"" "A6"]}]
                      :replace #{"Assault Rifle"}
                      :cost 10}
                     {:equipment [{:name "Heavy Flamethrower"
                                   :stats ["12\"" "A6" "AP(1)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 15}
                     {:equipment [{:name "Fusion Rifle"
                                   :stats ["12\"" "A1" "AP(4)" "Deadly(6)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 20}
                     {:equipment [{:name "Heavy Fusion Rifle"
                                   :stats ["24\"" "A1" "AP(4)" "Deadly(6)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 45}
                     {:equipment [{:name "Gravity Cannon"
                                   :stats ["24\"" "A4" "Rending"]}] 
                      :replace #{"Assault Rifle"}
                      :cost 20}
                     {:equipment [{:name "Heavy Machinegun"
                                   :stats ["36\"" "A3" "AP(1)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 25}
                     {:equipment [{:name "Plasma Cannon"
                                   :stats ["36\"" "A3" "Blast(3)" "AP(2)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 30}
                     {:equipment [{:name "Laser Cannon"
                                   :stats ["48\"" "A3" "AP(4)" "Deadly(3)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 45}
                     {:equipment [{:name "Missile Launcher"
                                   :stats [" * HE" "48\"" "A1" "Blast(3)" "<br/>"
                                           " * AT" "48\"" "A1" "AP(3)" "Deadly(3)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 35}]
                "G" [{:upgrade {:name "Veteran Infantry"
                                :stats ["Veteran Infantry"]}
                      :cost 10}
                     {:equipment {:name "Jetpack"
                                  :stats ["Flying" "Ambush"]}
                      :cost 15}
                     {:equiment {:name "Flamethrower"
                                 :stats ["12\"" "A6"]}
                      :replace #{"Pistol"}
                      :cost 10}
                     {:equiment {:name "Heavy Chainsaw Sword"
                                 :stats ["A6" "AP(1)"]}
                      :replace #{"Pistol" "CCW"}
                      :cost 10}]
                "H" [{:equipment [{:name "Heavy Flamethrower"
                                   :stats ["12\"" "A6" "AP(1)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 20}
                     {:equipment [{:name "Gravity Cannon"
                                   :stats ["24\"" "A4" "Rending"]}] 
                      :replace #{"Assault Rifle"}
                      :cost 25}
                     {:equipment [{:name "Heavy Machinegun"
                                   :stats ["36\"" "A3" "AP(1)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 30}
                     {:equipment [{:name "Plasma Cannon"
                                   :stats ["36\"" "A3" "Blast(3)" "AP(2)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 35}
                     {:equipment [{:name "Missile Launcher"
                                   :stats [" * HE" "48\"" "A1" "Blast(3)" "<br/>"
                                           " * AT" "48\"" "A1" "AP(3)" "Deadly(3)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 40}
                     {:equipment [{:name "Heavy Fusion Rifle"
                                   :stats ["24\"" "A1" "AP(4)" "Deadly(6)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 50}
                     {:equipment [{:name "Laser Cannon"
                                   :stats ["48\"" "A3" "AP(4)" "Deadly(3)"]}]
                      :replace #{"Assault Rifle"}
                      :cost 50}]}}))
                     
